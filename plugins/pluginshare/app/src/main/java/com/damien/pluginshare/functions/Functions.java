package com.damien.pluginshare.functions;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.Calendar;

/**
 * Created by Damien on 07/03/2016.
 */
public class Functions {

    /**
     * Get date to YYYYMMDD
     * @return String
     */
    static public String getYYYYMMDD(){
        final Calendar c = Calendar.getInstance();

        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH)+1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        return Integer.toString(mYear)+ Integer.toString(mMonth)+ Integer.toString(mDay);
    }

    /**
     * Get date to YYYYMMDD from dd/mm/yyyy
     * @param dd_mm_yyyy
     * @return String
     */
    static public String getYYYYMMDD(String dd_mm_yyyy) {
        if(dd_mm_yyyy == null || dd_mm_yyyy.length() < 10) return null;

        String jour;
        String mois;
        String annee;

        String[] tab = dd_mm_yyyy.split("/");

        if(tab.length == 3){
            jour = tab[0];
            mois = tab[1];
            annee = tab[2];
        }else return null;

        return annee+mois+jour;
    }

    /**
     *
     * @param YYYYMMDD
     * @return
     */
    static public String getDD_MM_YYYYFromYYYYMMDD(String YYYYMMDD) {
        try
        {
            if (YYYYMMDD.length()!=8)
                return "";

            String year=YYYYMMDD.substring(0, 4);
            String month=YYYYMMDD.substring(4, 6);
            String day=YYYYMMDD.substring(6, 8);

            return day+"/"+month+"/"+year;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    static public boolean checkNetworkConnection(Context context){
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
